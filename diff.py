import json
from jsondiff import diff


def diff_json(json_file_1, json_file_2):
    with open(json_file_1, 'r') as data_1:
        json_1_data = data_1.read()

    with open(json_file_2, 'r') as data_2:
        json_2_data = data_2.read()

    # parse file
    json_1_dict = json.loads(json_1_data)
    json_2_dict = json.loads(json_2_data)

    res = diff(json_1_dict, json_2_dict, syntax='explicit', load=False, dump=True)

    with open("data_res.json", "w") as outfile:
        outfile.write(res)
    print("data_res.json created")


json_file_2 = input('insert a filename of the json file as basis (ex. basis.json): ')
json_file_1 = input('insert a filename of the json file to compare (ex. compare.json): ')

diff_json(json_file_1, json_file_2)

# How to check differences between 2 JSON files

## Abstract 
It's very easy to check the differences between 2 JSON files through [jsondiff]( https://github.com/xlwings/jsondiff).
The result is written on another JSON file.

**This snippet DOESN'T MERGE the JSON files in a new one,** but it just show the differences between them.

## Examples
    $ python diff.py 

    $ insert a filename of the json file as basis (ex. basis.json): data1.json

    $ insert a filename of the json file to compare (ex. compare.json): data2.json

    $ data_res.json created



## Premise
This project requires :

* Python 3.8
* [jsondiff]( https://github.com/xlwings/jsondiff)

They have to be up and running. 


## Installation
* The root folder of the project is 
 
    >**diff_json** 
    
* and the absolute path is
 
    >**/var/www/html/diff_json**
    
* Clone the project from gitlab on your Desktop :
    
    >**git@gitlab.com:greengo_axe/diff_json.git**

* copy and overwrite all folders and files from 

>>**Desktop/diff_json**

>into 

>>**/var/www/html/diff_json**

* Delete the folder on your Desktop (**Desktop/diff_json**)


* Enjoy!


## F.A.Q

## Troubleshooting
 
## TODO List:
 * Whatever you like
